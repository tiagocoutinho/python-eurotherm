#!/usr/bin/env python
import time
import eurotherm
import sys

for port in ("/dev/ttyAMA0", "/dev/ttyUSB0", "/dev/ttyUSB1", "/dev/ttyUSB2","EXIT"):
    if port == 'EXIT':
        time.sleep(3)
        sys.exit()
    try:
        myEuro = eurotherm.eurotherm2408(port)
        print("Connecte sur "+port)
        break
    except Exception as e:
        print("Pas d'equipement connecte sur "+port)
        
    
print("Tension actuelle de sortie sur eurotherm : " +str(myEuro._1A_Maximum_electrical_output/10.0 ))
try:
    tension=float(raw_input('Tension de sortie eurotherm (pour 100%):'))
    
    myEuro.Instrument_Mode = 2
    myEuro._1A_Maximum_electrical_output = tension*10.0
    myEuro.Instrument_Mode = 0

except ValueError:
    print "Not a number"
    

