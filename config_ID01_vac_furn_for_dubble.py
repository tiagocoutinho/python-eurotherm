import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
qapp = qt.QApplication([])

keepgoing = True

def printTemp(myEuro):
    print ("Temperature via callback {temp}".format(temp=myEuro.temperature)) 


def rampTest(myEuro,updateThread):
    global keepgoing
    myEuro.Ramp_Rate_Disable = 0
    myEuro.rampRate = 30
    
    startingTemp = 50
    maxTemp = 950
    step = 50
    
    maxTime = 5*60

    while(keepgoing):
        myEuro.rampRate = 30
        myEuro.setpoint = startingTemp - 10
        starttime=time.time()
        tempstart = myEuro.temperature
        while myEuro.temperature  > startingTemp:
            rate = (myEuro.temperature - startingTemp)/((time.time()-starttime)/60.0)
            print("Cooling", startingTemp, str(myEuro.temperature), str(rate))
            updateThread.pidPlot("Cooling at {rate}deg/min".format(rate=rate))
            time.sleep(10)
        for s in range(startingTemp, maxTemp, step):
                starttime=time.time()
                myEuro.setpoint = s
                time.sleep(maxTime)
                updateThread.pidPlot("T={temp} degC".format(temp=myEuro.temperature))
        myEuro.rampRate = 50
                


def findP(myEuro, updateThread, maxTime=600, startingP = 1450, endingP = 100, stepP=-50, targetTemp=100 ):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
        myEuro.setpoint = targetTemp
            
        for p in range(startingP, endingP, stepP):
            if not keepgoing: break
            print("P", p)
            myEuro.P = p
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid)
        updateThread.pidPlot()
        print("Found p ! ",str(myEuro.pid))
    except Exception as e :
        print("findP crashed",str(e))

    
def findD(myEuro, updateThread, maxTime=600, startingD = 0, endingD = 200, stepD=10, targetTemp=100 ):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
        myEuro.setpoint = targetTemp
            
        for d in range(startingD, endingD, stepD):
            if not keepgoing: break
            print("D", d)
            myEuro.D = d
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid)
        updateThread.pidPlot()
        print("Found d ! ",str(myEuro.pid))
    except Exception as e :
        print("findP crashed",str(e))

    
def findI(myEuro, updateThread, maxTime=600, startingI = 0, endingI = 200, stepI=10, targetTemp=100 ):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
        myEuro.setpoint = targetTemp
            
        for i in range(startingI, endingI, stepI):
            if not keepgoing: break
            print("I", i)
            myEuro.I = i
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid)
        updateThread.pidPlot()
        print("Found i ! ",str(myEuro.pid))
    except Exception as e :
        print("findI crashed",str(e))



        
def shakeAndFindP(myEuro, updateThread, maxTime=600, startingP = 1450, endingP = 100, stepP=-50, stepTemp=5):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 0
        
            
        for p in range(startingP, endingP, stepP):
            myEuro.setpoint = myEuro.setpoint + stepTemp
            
            if not keepgoing: break
            print("P", p)
            myEuro.P = p
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid, myEuro.setpoint)
        updateThread.pidPlot()
        print("Found p ! ",str(myEuro.pid))
    except Exception as e :
        print("findP crashed",str(e))

    
def autotuneRamp(myEuro, updateThread, startingTemp = 50, endingTemp = 500, step=50):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
            
        for temp in range( startingTemp , endingTemp , step):
            myEuro.setpoint = temp
            
            if not keepgoing: break

            print("Autotune start for {temp}".format(temp=temp))
            myEuro.Autotune_enable = 1

            time.sleep(10)
            auto = 1
            while auto and keepgoing:
                try:
                    auto = myEuro.Autotune_enable
                    if auto == 0:
                        time.sleep(5)
                        auto = myEuro.Autotune_enable
                except Exception as e:
                    print("Pbl while reading autotune parameters",str(e))
                time.sleep(1)

            updateThread.pidPlot()        
            print("Autotune done for {temp}, pid={pid}".format(temp=temp, pid=str(myEuro.pid)))

            time.sleep(60)

            
    except Exception as e :
        print("Autune ramp crashed",str(e))

def manualTempVsTension(myEuro, updateThread):    
    # Definition of the tension vs max temperature:
    
    myEuro.manual = True
    myEuro.power = 0

    updateThread.updateTime=5
    
    while myEuro.temperature > 40.0:
        time.sleep(10)
        print("Cooling to 40 degC", myEuro.temperature)

    updateThread.updateTime=0.5

    for power in range(0,99,1):
        myEuro.power = power/10.0
        time.sleep(60)
        print("power output vs temperature", power/10.0, myEuro.temperature)
        updateThread.pidPlot(text=str(power/10.0))
        

    
    myEuro.power = 0

    
    
if __name__ == "__main__":

    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    myEuro.Adaptive_tune_enable=0
    myEuro.Autotune_enable = 0




    global app
    app = qt.QApplication([])


    mySilxEurotherm = eurotherm.silxEurotherm(myEuro)
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    updateThread = eurotherm.UpdateThread(mySilxEurotherm, updateTime=0.1)

    # open silx window
    mySilxEurotherm.show()
    
    updateThread.start()  # Start updating the plot

    time.sleep(2)
    updateThread.pidPlot()

    print(myEuro.temperature)


    
    
    # Furnace for Dubble, borrowed from ID01.
    # maxRetran = 45
    # Tension hard limit = 9.0V
    # Current max 4.5 A

    # 8.6V - 4.3A => 945 deg env
    # 9.0V 4.4A  => 980 deg C env
  
    
    # myEuro.pid = (342, 30, 5)
    # myEuro.rampRate = 30
    # myEuro.setpoint = 200

    
    # ramp = threading.Thread(target=rampTest, args=(myEuro,updateThread,))
    # ramp.start()
    # print("ramp.join() ;keepgoing=False  ; # To stop it  ")
      
