import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
def gui():
    
    global app
    app = qt.QApplication([])

    time.sleep(1)
    global mySilxEurotherm
    mySilxEurotherm = eurotherm.silxEurotherm(myEuro)
    time.sleep(1)
    
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    global updateThread
    updateThread = eurotherm.UpdateThread(mySilxEurotherm, updateTime=0.1)

    time.sleep(1)
    # open silx window
    mySilxEurotherm.show()
    time.sleep(1)
    
    updateThread.start()  # Start updating the plot

    time.sleep(1)
    
    updateThread.pidPlot()

    print(myEuro.temperature)


if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    myEuro.Adaptive_tune_enable=0
    myEuro.Autotune_enable = 0

    

    #gui()


    # Request, microtomo Hwl in transmission RT-550 deg Hydrogen flow for Alessandro. 
    # Will use 5 or 10 deg ramprate.
    
    
    # For microtomo :
    # Max V/I : 7,4V 11A 
    # myEuro.tensionRange = (0,5)
    # Alessandro need max 550degC and most exp at 300 in Hydrogen, moly head.
    # Eurotherm make a tension gap between 99 and 100% This screw regulation close to max tension. Better to keep the full range of (0,5), and regulate the max current
    # Delta topped at 9.0A (550++)
    
    
    
    myEuro.Output_rate_limit = 1000

    # Previous Values :
    #myEuro.pid =(29, 143, 23) # at 100deg
    #myEuro.pid = (71, 101, 16) # at 500 degC
    # myEuro.pid =(60, 400, 40) # Previous request from dubble
    # myEuro.pid = (49, 101, 23) # ok at 500 good compromise for RT-500 ... 1.2 deg overshoot even at 10deg/min


    #####  GOOD ONES : ######
    # # Tension range (0, 5) # 9A max on PS.
    # #Output_rate_limit = 1000
    myEuro.pid = (33, 60, 10) # @ 550degC 
    # At 200C, +/-0.03K, few peaks of 40mK
    # undershoot of 3C when going down.
    
    

    

    #myEuro.rampRate = 50 # 
    myEuro.rampRate = 25 # 
    
    # pictures:
    # microtomo_hwl_dubble_air_500degC_ramp10.png
