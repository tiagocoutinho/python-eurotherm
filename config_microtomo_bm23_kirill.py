import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB1")

    # Request, microtomo Hwl in transmission RT-600 deg in gas flow large diam sample holder hole.
    # As backup for the IR furnace
    
    myEuro.Output_rate_limit = 2000


    #####  GOOD ONES : ######
    # microtomo :
    # Max V/I : 7,4V 11A
    #
    # Tension range (0, 5) # 9A max on PS.
    # myEuro.tensionRange = (0,5) # 7.1V (99% => 6.8V)
    # myEuro.tensionRange = (0,6) # 8.4V (+Hard lim a 7.2V) (99% 8.2V) <<<<<<<
    #
    # #Output_rate_limit = 2000
    myEuro.pid = (33, 60, 10) # @ 550degC 
    # At 200C, +/-0.03K, few peaks of 40mK
    # undershoot of 3C when going down.
    
    

    

    myEuro.rampRate = 25 # 
