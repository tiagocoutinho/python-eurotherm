import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")

    
    myEuro.tensionRange = (0, 100)  ## No limitation, 0-10V


    ## vis M12
    myEuro.pid = (100, 21, 10)
    # 210C : +/-0.1C  (Full range 0-10V)
    
    # AT @210C with full range 0-10V
    myEuro.pid = (65, 54, 9)

    # Slower but no overshoot: 
    myEuro.pid = (100, 54, 9)
    
    # Vis M12
    # 5% ~~ 220C
    
