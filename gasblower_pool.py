import time
import eurotherm

if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")
    #myEuro = eurotherm.eurotherm2408("/dev/ttyUSB1")


    # To reclean a mini gas blower :
    # myEuro.Output_rate_limit = 2000
    # myEuro.rampUnit = 'Seconds' # to be checked per hot air blower
    # myEuro.rampRate = 30
    # myEuro.pid = (4000, 112, 0)
    # myEuro.pid2 = (4000, 112, 0)
    
    # Initial values on the normal gas blower n1
    # myEuro.pid =   (6.6, 23.7, 0.0)
    # myEuro.pid2 =  (2.0, 36.0, 6.0)
    # myEuro.rampRate = 10
    # myEuro.Output_rate_limit = 0

    # myEuro.Instrument_Mode = 0.2
    # myEuro.Output_rate_limit = 2000
    # myEuro.Instrument_Mode = 0
    # myEuro.floatingPointDataFormat = 0 #>

    # Fun fact with Eurotherm #
    # To enter an eurotherm with floatingPointDataFormat as integer, one must do:
    # myEuro.Instrument_Mode = 0.2  (not 2)
    # myEuro.AA_Comms_Resolution = 0   (to go back in the blissed mode of floating points)
    
    

    # pid = (20, 0, 0) # Ok at 200degC ramp 100=>200 ok. No overshoot. 64sec oscilation period during ramp. 79 sec at 200C
    # myEuro.pid = (10,80,0)  # ok at 200C
    # With P=10  => ramp has overshoot.

    # Actual pid, to be continued :
    # myEuro.pid = (20,160,0) @ 300degC undershoot 
    # myEuro.pid = (30,160,0) @ 350degC Slower to get there, but goes 
 


    # Thermocouple calibration is wrong, temperature out from the nozzle of the blower is higher than setpoint and read temperature
    # Seen on Id11 end January 2017.
    # Attempting a calibration of the offet.
    # Thermocouple K  at the nozzle  / thermocouple S inside the blower / Difference.


    # some cherrypicked default Parameters:
    # myEuro.tensionRange = (0, 100)

    






def dumpit():
    import pickle;
    dumped = myEuro.dumpAll()
    sauvegarde = file("/home/pi/blower1_allOriginalParameters.p", 'w');
    pickle.dump(dumped, sauvegarde);
    sauvegarde.close()
    print (str(dumped))




def sauvegarde():
    import pickle;
    offsets.append((myEuro.temperature, myEuro2.temperature, myEuro.temperature-myEuro2.temperature))
    sauvegarde = file("/home/pi/blower_offsets.p", 'w');
    pickle.dump(offsets, sauvegarde);
    sauvegarde.close()
    print (str(offsets))
    # (22.12203025817871, 21.600299835205078)
    # (86.18880462646484, 75.07308959960938, 11.089630126953125)
    
    # Custom mV ?


# from euroTools import compareTwoDict
# compareTwoDict(dumped, new)
# ('User_calibration_adjust_input_1', ':  ', 201, ' =/= ', 1)
# ('_2A_Module_function', ':  ', 1, ' =/= ', 0)
# ('_2A_Summary_output_2A_configuration', ':  ', 1, ' =/= ', 0)
# ('_2A_Summary_output_2A_configuration', ':  ', 1, ' =/= ', 0)
# ('_2A_Maximum_electrical_output', ':  ', 1, ' =/= ', 0)
# ('_2A_Maximum_electrical_output', ':  ', 1, ' =/= ', 0)
# ('_2A_Units', ':  ', 64, ' =/= ', 0)
# ('_2A_Module_identity', ':  ', 3, ' =/= ', 0)
# ('_2A_PID_or_Retran_high_value', ':  ', 65535, ' =/= ', 0)


#  `('_1A_Maximum_electrical_output', ':  ', 100, ' =/= ', 50)
# ('_1B_Summary_of_1B_configuration', ':  ', 0, ' =/= ', 32)
# ('_1C_Summary_of_1C_configuration', ':  ', 0, ' =/= ', 64)
# ('_1C_Module_1C_value_giving_max_output', ':  ', 0, ' =/= ', 1)


# Difference entre nos controlleur du pool et le controlleur du gas blower
# pool : H8 Reg Inverse 0-5V
# blower HZ Reg inverse 0-10V

# Gas blower LC: Reg PID directe (logique non isolee)
