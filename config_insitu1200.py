
import templogger
import matplotlib.pyplot as plt
import time
import eurotherm


def printTemp(myEuro):
    print ("Temperature via callback {temp}".format(temp=myEuro.temperature)) 


if __name__ == "__main__":

#    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB1")
    myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0", baudrate=9600)
    
    # myEuro.Ramp_Rate_Disable = 1
    # time.sleep(0.5)
    # myEuro.setpoint = 20
    # time.sleep(0.5)

    # Configuration for ID26 inhouse.
    # Rafal's need :
    #     activation of sample at 450C
    #     Stability at 350C

    # Need 35V 1.8A to get 450degC (just, with a plateau to arrive there, and no gas flow in the cell... )

    if False :  # Only one time to enforce configuration of eurotherm
        #('BEFORE', 12, 17, 0, 100, 1, 0, 5, 0, 0, 0, 0)
        print("BEFORE", myEuro._1A_Module_identity,\
            myEuro._1A_Module_function,\
            myEuro._1A_PID_or_Retran_value_giving_min__o_p,\
            myEuro._1A_PID_or_Retran_value_giving_max__o_p,\
            myEuro._1A_Units,\
            myEuro._1A_Minimum_electrical_output,\
            myEuro._1A_Maximum_electrical_output,\
            myEuro._1A_Sense_of_output,\
            myEuro._1A_Summary_output_1A_configuration,\
            myEuro._1A_DC_output_1A_telemetry_parameter,\
            myEuro._1A_Program_summary_output_1A_config)

        #('BEFORE', None, 17, 0, 29, 1, 0, 50, 0, 0, 0, 0)
        # myEuro._1A_PID_or_Retran_value_giving_max__o_p  # Tension on DE "70V"
        #   0% :  0.0V
        #   1% : 72.2V
        #  31% : 22.7V
        #  50% : 14.0V
        #  75% :  9.3V
        # 100% :  6.9V

        desiredMaxTension = 23.0 # V
        onePcOoutput = 72.2      # V
        hundredPcOutput = 6.9    # V
        
        guess = int( (onePcOoutput - desiredMaxTension) * (onePcOoutput - hundredPcOutput)/99.0) - 2
        
        myEuro._1A_PID_or_Retran_value_giving_min__o_p = 0
        myEuro._1A_PID_or_Retran_value_giving_max__o_p = guess
        
        time.sleep(1)
        myEuro.tensionRange = (0.0,5.0) # Delta electronika's are in 0-5V not 0-10V 
        myEuro.Low_power_limit = 0
        myEuro.High_power_limit = 100
        myEuro.Remote_low_power_limit = 0
        myEuro.Remote_high_power_limit =100

        

        
        
        time.sleep(1)        
        print("AFTER ",myEuro._1A_Module_identity,\
            myEuro._1A_Module_function,\
            myEuro._1A_PID_or_Retran_value_giving_min__o_p,\
            myEuro._1A_PID_or_Retran_value_giving_max__o_p,\
            myEuro._1A_Units,\
            myEuro._1A_Minimum_electrical_output,\
            myEuro._1A_Maximum_electrical_output,\
            myEuro._1A_Sense_of_output,\
            myEuro._1A_Summary_output_1A_configuration,\
            myEuro._1A_DC_output_1A_telemetry_parameter,\
            myEuro._1A_Program_summary_output_1A_config)
        time.sleep(1)
    # Tests for ID03 (Linus) Needs 200degC without the cover on top.
    # Ok but slow to get there (600 / 131-68):    (480, 94, 23) +/- 0.08C

    
    pidPonly = False
    ponly = 600
    tou = 131-68 # "Good gain method"  Time between first over and first undershooting (not the period)
    #    myEuro.pid = (ponly*0.8, 1.5*tou, 0)
    if pidPonly:
        myEuro.pid = (ponly,0,0)
    else:
        myEuro.pid = (ponly*0.8, 1.5*tou, 1.5*tou/4.0)

    print(str(myEuro.pid))

    ramp = 10 # 10
    if myEuro.rampRate != ramp:
        myEuro.rampRate = ramp # 40 

    startingTemp = myEuro.temperature

    
    targetTemp = 200

#    myEuro.setpoint = startingTemp
    
    # while myEuro.temperature > startingTemp:
    #     print(startingTemp, str(myEuro.temperature))
    #     time.sleep(1)
    #myEuro.Ramp_Rate_Disable = 0
    #myEuro.Autotune_enable = 1

    
    #time.sleep(10)
    #print(" Adaptive tuning" )
    #myEuro.Adaptive_tune_enable=0
    #myEuro.Adaptive_tune_trigger_level=0

    
    if myEuro.setpoint != targetTemp:
        myEuro.setpoint = targetTemp

    time.sleep(1)
    
    mylogger = templogger.logger(myeurotherm=myEuro,sleepTime=0.1, showPower=False)

    #mylogger.callbacks.append(printTemp(myEuro))
    
    plt.show()
    
    # myEuro.Ramp_Rate_Disable = 1
    # time.sleep(0.5)
    # myEuro.setpoint = 20
    # time.sleep(0.5)
    # myEuro.Ramp_Rate_Disable = 0
    # time.sleep(0.5)

    
    print(str(myEuro.pid))

    #myEuro.Adaptive_tune_enable=0
    
    #plt.savefig(time.strftime("%Y-%m-%d_%H-%M-%S_ID24_plot.png"))
    #mylogger.saveData()
    
    #mylogger.myEuro.setpoint =20
    
