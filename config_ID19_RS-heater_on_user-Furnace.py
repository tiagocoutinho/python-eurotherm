import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
def gui():
    
    global app
    app = qt.QApplication([])

    time.sleep(1)
    global mySilxEurotherm
    mySilxEurotherm = eurotherm.silxEurotherm(myEuro)
    time.sleep(1)
    
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    global updateThread
    updateThread = eurotherm.UpdateThread(mySilxEurotherm, updateTime=0.1)

    time.sleep(1)
    # open silx window
    mySilxEurotherm.show()
    time.sleep(1)
    
    updateThread.start()  # Start updating the plot

    time.sleep(1)
    
    updateThread.pidPlot()

    print(myEuro.temperature)


if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    #myEuro.Adaptive_tune_enable=0
    #myEuro.Autotune_enable = 0


    # RS heater for custom humid air blower (temp approx 200C) (User furnace)

    # myEuro.tensionRange = (0, 50)  ## Need max power as it is a 240V heater from RS.
    
    # @175C 150mK overshoot @185 400mK overshoot
    myEuro.pid = (7, 100, 16)
    
    
    myEuro.rampRate = 10

    # Images: 
    # id19_humidity_furnace_200degC_01.png
    # id19_humidity_furnace_200degC_02.png
