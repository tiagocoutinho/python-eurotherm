import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
qapp = qt.QApplication([])

keepgoing = True
    
    
if __name__ == "__main__":

    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    myEuro.Adaptive_tune_enable=0
    myEuro.Autotune_enable = 0




    global app
    app = qt.QApplication([])


    mySilxEurotherm = eurotherm.silxEurotherm(myEuro)
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    updateThread = eurotherm.UpdateThread(mySilxEurotherm, updateTime=0.1)

    # open silx window
    mySilxEurotherm.show()
    
    updateThread.start()  # Start updating the plot

    time.sleep(2)
    updateThread.pidPlot()

    print(myEuro.pid)


    # Restriction to 14V / 8A (800C) :
    
    #myEuro.tensionRange = (0,9)

    # Restriction to 5.8V  for 360C
    #myEuro.tensionRange = (0,3)
    #myEuro.Output_rate_limit = 2000
    
    #  @200 in air: 
    myEuro.pid = (12, 104, 17)  # Good at (0,3) tension range, up to 300 regulated
    
    # Restriction to 8.6V  for 4xx C
    #myEuro.tensionRange = (0,5)
    #myEuro.Output_rate_limit = 2000
    #myEuro.setpoint = 200

    
