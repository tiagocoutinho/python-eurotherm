import templogger
import matplotlib.pyplot as plt
import time
import eurotherm


def printTemp(myEuro):
    print ("Temperature via callback {temp}".format(temp=myEuro.temperature)) 


if __name__ == "__main__":

    myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")
    
    myEuro.Ramp_Rate_Disable = 1
    time.sleep(0.5)
    myEuro.setpoint = 20
    time.sleep(0.5)

    print(str(myEuro.pid))

    startingTemp = myEuro.temperature
    targetTemp = 600
    
    while myEuro.temperature > startingTemp:
        print(startingTemp, str(myEuro.temperature))
        time.sleep(1)
    myEuro.setpoint = startingTemp
    myEuro.Ramp_Rate_Disable = 0
    myEuro.Autotune_enable = 0

    # Delta is at 19V 10A Tension regulation
    #myEuro.pid = (100,0,0) #stable at 188 for 200
    #myEuro.pid = (50,0,0)  # stable at 194 for 200 +/- 1 deg oscilation
    #myEuro.pid = (37,0,0)  # oscilate, no overshoot. -6/-2
    #myEuro.pid = (25,0,0)  # oscilate -6 +0 (first overshoot at +2)
    #myEuro.pid = (12,0,0)  # oscilate, +3/-3   (+1 overshoot, 
    #myEuro.pid = (14,0,0)  # oscilate, +3/-4
    #myEuro.pid = (10,0,0)   # oscilate -3/+4

    # best P = 12 at 300degC
    # oscilation period = 34 sec.
    # first Estimate for a pid:
    # P: 1.7xP = 20.4
    # I: 0.5x34 = 17
    # D: 0.12x34 = 4.08
    #myEuro.pid = (20,17,4) # +/-1 deg oscilations with 20sec oscilations at 200degC

    #tests at 300degC
    #myEuro.pid = (20,17,4) # +/-3 deg oscilations with 26sec oscilations at 300degC
    # Autotune 20,26,4 #+/-3 deg oscilations
    #myEuro.pid = (20,9,4) # +3/-3 deg
    #myEuro.pid = (20,90,2)  # take long time to decreas the initial overshoot while oscilating, then +3/-2
    #myEuro.pid = (20,90,10) # not too long then +3/-2
    #myEuro.pid = (20,20,4) # not too long then +3/-2

    # test at 400degC
    myEuro.pid = (18,17,4) # +/- 4deg
    #myEuro.pid = (17,17,4) # +5/-3 
    
    
    #time.sleep(10)
    #print(" Adaptive tuning" )
    myEuro.Adaptive_tune_enable=0
    #myEuro.Adaptive_tune_trigger_level=0
    
    
    mylogger = templogger.logger(eurotherm=myEuro,sleepTime=0.5, showPower=False)
#    mylogger = templogger.logger(eurotherm=myEuro,sleepTime=0.5, showPower=True)

    #mylogger.callbacks.append(printTemp(myEuro))
    
    myEuro.rampRate = 10 # 23?
    myEuro.setpoint = targetTemp
    
    plt.show()
    
    # myEuro.Ramp_Rate_Disable = 1
    # time.sleep(0.5)
    # myEuro.setpoint = 20
    # time.sleep(0.5)
    # myEuro.Ramp_Rate_Disable = 0
    # time.sleep(0.5)

    
    print(str(myEuro.pid))

    myEuro.Adaptive_tune_enable=0
    
    #plt.savefig(time.strftime("%Y-%m-%d_%H-%M-%S_ID24_plot.png"))
    #mylogger.saveData()
    
    #mylogger.myEuro.setpoint =20
    
