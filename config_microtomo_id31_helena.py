import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
def gui():
    
    global app
    app = qt.QApplication([])

    time.sleep(1)
    global mySilxEurotherm
    mySilxEurotherm = eurotherm.silxEurotherm(myEuro)
    time.sleep(1)
    
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    global updateThread
    updateThread = eurotherm.UpdateThread(mySilxEurotherm, updateTime=0.1)

    time.sleep(1)
    # open silx window
    mySilxEurotherm.show()
    time.sleep(1)
    
    updateThread.start()  # Start updating the plot

    time.sleep(1)
    
    updateThread.pidPlot()

    print(myEuro.temperature)


if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB1")
    myEuro.Adaptive_tune_enable=0
    myEuro.Autotune_enable = 0

    

    #gui()


    # Request, 2x microtomo Hwl, one in transmission (Ni large opening), another in reflexion (Ni flat) RT-400 deg in air for Helena on ID31. 
    
    
    
    # For microtomo :
    # Max V/I : 7,4V 11A 
    # myEuro.tensionRange = (0,5)
    # Delta topped at 7.0A (?)
    # myEuro.Output_rate_limit = 1000

    #####  GOOD ONES : ######
    # # Tension range (0, 5) # 9A max on PS.
    # Four en transmission, Ni 
    # 1V 2.4A  100degC
    # 2.3V 5.1A  300 degC
    # Four reflection
    #1.3V 2.4A 100degC
    
    #
    myEuro.Output_rate_limit = 1000
    myEuro.pid = (33, 60, 10) 
    
    

    

    #myEuro.rampRate = 50 # 
    myEuro.rampRate = 25 # 
    
