import templogger
import matplotlib.pyplot as plt
import time
import eurotherm


def printTemp(myEuro):
    print ("Temperature via callback {temp}".format(temp=myEuro.temperature)) 


if __name__ == "__main__":

    myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")

    print("Previous "+str(myEuro.pid))
    
    if False :  # Only one time to enforce configuration of eurotherm

        print(myEuro._1A_Module_identity,\
            myEuro._1A_Module_function,\
            myEuro._1A_PID_or_Retran_value_giving_min__o_p,\
            myEuro._1A_PID_or_Retran_value_giving_max__o_p,\
            myEuro._1A_Units,\
            myEuro._1A_Minimum_electrical_output,\
            myEuro._1A_Maximum_electrical_output,\
            myEuro._1A_Sense_of_output,\
            myEuro._1A_Summary_output_1A_configuration,\
            myEuro._1A_DC_output_1A_telemetry_parameter,\
            myEuro._1A_Program_summary_output_1A_config)

        # Initial furnace table 1 with 0-100%
        # (12, 17, 0, 100, 1, 0, 50, 0, 0, 0, 0)
        # (12, 17, 0, 100, 1, 0, 50, 0, 0, 0, 0)
        # after auto config
        # (12, 17, 0, 25, 1, 0, 50, 0, 0, 0, 0)

        
        # Eurotherm in manual full output.
        # Give the max tension:
        # value = 1; myEuro.Instrument_Mode = 2; myEuro._1A_PID_or_Retran_value_giving_max__o_p = value; myEuro.Instrument_Mode = 0
        # if too low (should be close to max):
        # myEuro.tensionRange = (0.0,50.0)
        # Give the min tension:
        # value = 100; myEuro.Instrument_Mode = 2; myEuro._1A_PID_or_Retran_value_giving_max__o_p = value; myEuro.Instrument_Mode = 0
        

        # Thermocoax version : 60-180-4   31.5V 1A
        # Furnace table 1
        
        desiredMaxTension = 31.5 # V
        onePcOoutput = 72.6 # 72.2      # V
        hundredPcOutput = 6.9    # V
        
        guess = int( (onePcOoutput - desiredMaxTension) * (onePcOoutput - hundredPcOutput)/99.0) - 2
        myEuro.Instrument_Mode = 2
        myEuro._1A_PID_or_Retran_value_giving_min__o_p = 0
        #myEuro._1A_PID_or_Retran_value_giving_max__o_p = guess
        myEuro._1A_PID_or_Retran_value_giving_max__o_p = 23

        # 23 for 31.5V for thermocoax.
        # 90 for microtomo HWL in tension regulation  ('BEFORE', None, 17, 0, 90, 1, 0, 50, 0, 0, 0, 0)
        myEuro.Low_power_limit = 0
        myEuro.High_power_limit = 100
        myEuro.Remote_low_power_limit = 0
        myEuro.Remote_high_power_limit =100
        myEuro.Instrument_Mode = 0
        time.sleep(8)
        myEuro.tensionRange = (0.0,50.0) # Why 50 ?  

        
        print(myEuro._1A_Module_identity,\
            myEuro._1A_Module_function,\
            myEuro._1A_PID_or_Retran_value_giving_min__o_p,\
            myEuro._1A_PID_or_Retran_value_giving_max__o_p,\
            myEuro._1A_Units,\
            myEuro._1A_Minimum_electrical_output,\
            myEuro._1A_Maximum_electrical_output,\
            myEuro._1A_Sense_of_output,\
            myEuro._1A_Summary_output_1A_configuration,\
            myEuro._1A_DC_output_1A_telemetry_parameter,\
            myEuro._1A_Program_summary_output_1A_config)

        time.sleep(100)
        
    # test at 150 degC
    # 500 : Oscilate and settle  76sec tou
    # 600 : goes down slowly
    # 450 :  Oscilate
    # 400 : Oscilate
    # 500 + p + I @76sec tou:  Slow to reach setpoint (210sec) but stable
    # 100 + pi @76 vershoot + slow to stab
    # 200 +pi @ 76 17deg overshoot
    # 550 + pi @76 : Slow
    # 600 +pi ok a 150deg
    
    # test at 250 degC
    # 600+pi ok but 2 deg overshoot take 250sec to reach sp

    # test autotune p only : 260  Tou: 56/119 63
    # 208/94/23 : bof bof

    #Best today :(480, 114, 28) No overshoot, takes time to arrive at sp. Long term stability ok.


    # in vacuum, 100degC
    # p=500: stuck at 93
    # p=300: stuck at 95.3
    # p=200: stuck at 96.5/97.3
    # p=100: reach 100, oscilate +0.3/-2.5
    # p=50: reach 100, oscilate +2.5/-2.0
    # (400, 81, 20) : oscilate -0/+1.5
    # (40, 67, 16) oscilate -0/+1.5
    

    pidPonly = False
    goodGain = False
    ponly = 50 
    tou = 106-61 # 4367 # 4367? "Good gain method"  Time between first over and first undershooting (not the period)
    period = 100 # for eurotherm method
    #    myEuro.pid = (ponly*0.8, 1.5*tou, 0)
    if pidPonly:
        myEuro.pid = (ponly,0,0)
    else:
        if goodGain:
            myEuro.pid = (ponly*0.8, 1.5*tou, 1.5*tou/4.0) # GG method
        else: 
            myEuro.pid = (ponly*1.7, 0.5*period, 0.12*period) # Eurotherm Manual method

    # For thermocoax microtomo @ 100 degC under vacuum
    myEuro.pid = (418, 197, 32)


    print(str(myEuro.pid))

    
    ramp = 7 # 10
    if myEuro.rampRate != ramp:
        myEuro.rampRate = ramp # 40 

    targetTemp = 200
    
    # startingTemp = 150
    # while myEuro.temperature > startingTemp:
    #      print(startingTemp, str(myEuro.temperature))
    #      time.sleep(1)


    #myEuro.Ramp_Rate_Disable = 0
    #myEuro.setpoint = startingTemp

    # First autotune

    # Eurotherm autotune : set p&i&d high cutback=0 (auto) and lowcutback=0 (auto)
    # ask an higher temp than actual !
    # wait for 2 cycles of : max power / no power (20min approx)
    #myEuro.Autotune_enable = 1

    # Low and high cutback values in auto are 3xP . Way too much for us.
    # for autotune it has to be in auto (0).
    # Avoid over/undershoot:
    myEuro.cutbackHigh = 0
    myEuro.cutbackLow = 0

    # second autotune (adaptive)
    #time.sleep(10)
    # print(" Adaptive tuning" )
    # myEuro.Adaptive_tune_enable=0
    # myEuro.Adaptive_tune_trigger_level= 1


    #time.sleep(3)
    if myEuro.setpoint != targetTemp:
        myEuro.setpoint = targetTemp
    
    mylogger = templogger.logger(myeurotherm=myEuro,sleepTime=1, showPower=False)

    #mylogger.callbacks.append(printTemp(myEuro))
    
    plt.show()
    
    # myEuro.Ramp_Rate_Disable = 1
    # time.sleep(0.5)
    # myEuro.setpoint = 20
    # time.sleep(0.5)
    # myEuro.Ramp_Rate_Disable = 0
    # time.sleep(0.5)

    
    print(str(myEuro.pid))

    myEuro.Autotune_enable = 0
    myEuro.Adaptive_tune_enable=0
    
    #plt.savefig(time.strftime("%Y-%m-%d_%H-%M-%S_ID24_plot.png"))
    mylogger.saveData()
    
    #mylogger.myEuro.setpoint =20
    
