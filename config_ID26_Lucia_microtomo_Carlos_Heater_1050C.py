import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
def gui():
    
    global app
    app = qt.QApplication([])

    time.sleep(1)
    global mySilxEurotherm
    mySilxEurotherm = eurotherm.silxEurotherm(myEuro)
    time.sleep(1)
    
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    global updateThread
    updateThread = eurotherm.UpdateThread(mySilxEurotherm, updateTime=0.1)

    time.sleep(1)
    # open silx window
    mySilxEurotherm.show()
    time.sleep(1)
    
    updateThread.start()  # Start updating the plot

    time.sleep(1)
    
    updateThread.pidPlot()

    print(myEuro.temperature)


if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    #myEuro.Adaptive_tune_enable=0
    #myEuro.Autotune_enable = 0


    # Configuration of a new inhouse heater made of 1mm Kanthal wire inside a 1" button ceramic heater
    # Goal is to have a 1050 degC on a silicon sample
    # Geometry is reflection .

    # Heater is limitted at 18V  (13A) Regulation is tension.
    # myEuro.temperatureSensor = 'S'

    # myEuro.tensionRange = (0, 5)
    # myEuro.tensionRange = (0, 11) => 17V on DE
    # myEuro.tensionRange = (0, 12) => more than 18V needed.
    
    # myEuro.Instrument_Mode = 2;
    # myEuro.Setpoint_Max___High_range_limit = 1200
    # myEuro.Instrument_Mode = 0;

    # myEuro.Output_rate_limit = 0
    # myEuro.manual = False
    # myEuro.power = 0   
    
    # myEuro.Output_rate_limit = 1000
    # myEuro.rampRate = 20
    
    # myEuro.setpoint = 20    
    # myEuro.manual = False
    
    # myEuro.pid = (91, 664, 110) # Automatic at 100C

    # Good at high Temp :
    #myEuro.pid = (30, 25, 3)      # Alamano @800C
    
