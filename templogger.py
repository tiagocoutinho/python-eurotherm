import eurotherm
import pickle
import time
import matplotlib.pyplot as plt
import numpy
import threading
import datetime

class logger(threading.Thread):
    def __init__(self, myeurotherm=None, serialLine="/dev/ttyAMA0", sleepTime=1, showPower=False):
        threading.Thread.__init__(self)

        self.sleepTime = sleepTime
        if myeurotherm==None:
            self.myEuro = eurotherm.eurotherm2408(serialLine)
        else:
            self.myEuro = myeurotherm
            
        self.myEuro.debugPrint = False

        self.showPower=showPower
        self.fig = plt.figure()
        self.fig.add_subplot(111)
        self.fig.add_subplot(111)
        
        if self.showPower:
            self.myPlot,self.myPlot2,self.myPlot3,self.myPlot4 = plt.plot([], [], 'r+-', [], [], 'g+-',[], [], 'b+-', [],[],'b+-')
            self.myplots = (self.myPlot,self.myPlot2,self.myPlot3, self.myPlot4)
        else:            
            self.myPlot,self.myPlot2,self.myPlot3, = plt.plot([], [], 'r+-', [], [], 'g+-',[], [], 'b+-')
            self.myplots = (self.myPlot,self.myPlot2,self.myPlot3)


                
        self.daemon = True

        self.callbacks = []
        
        self.start()

        
    def update_line(self,new_datas):
        timeSinceStart = time.time() - self.startingTime

        print(timeSinceStart, new_datas)

        for (new_data, myPlot) in zip(new_datas, self.myplots):
            myPlot.set_xdata(numpy.append(myPlot.get_xdata(), timeSinceStart ))
            myPlot.set_ydata(numpy.append(myPlot.get_ydata(), new_data ))

        for myPlot in  self.myplots:
            myPlot.axes.relim()
            myPlot.axes.autoscale_view()
            
        
        plt.draw()

        for cb in self.callbacks:
            cb()
            

    def saveData(self, fileName=None):
        if fileName==None:
            fileName=time.strftime("%Y-%m-%d_%H-%M_measurment.p")
        try:
            out = []
            
            for myPlot in  self.myplots:
                out.append(myPlot.get_xdata())
                out.append(myPlot.get_ydata())
                
            pickle.dump(out, file(fileName, 'w'))
            
        except Exception as e:
            print("Impossible to save the configuration file of this eurotherm "+str(e))

        

    def run(self):
        self.startingTime = time.time()
        while True:
            try:
                before = time.time()

                #print pid
                print(self.myEuro.pid)
                
                if self.showPower:
                    self.update_line((self.myEuro.temperature, self.myEuro.setpoint, self.myEuro.workingSetpoint, self.myEuro.pc_Output_power) )
                else:
                    self.update_line((self.myEuro.temperature, self.myEuro.setpoint, self.myEuro.workingSetpoint) )

                duree = self.sleepTime - (time.time()-before)
                if duree>0:
                    time.sleep(duree)
            except Exception as e:
                print(str(e))
                break
                
if __name__ == "__main__":
    mylogger = logger()
    plt.show()
    plt.savefig(time.strftime("%Y-%m-%d_%H-%M-%S_plot.png"))
