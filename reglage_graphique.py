import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
if __name__ == "__main__":

    global myEuro

    for port in ("/dev/ttyAMA0", "/dev/ttyUSB0", "/dev/ttyUSB1", "/dev/ttyUSB2","EXIT"):
        if port == 'EXIT':
            time.sleep(3)
            sys.exit()
        try:
            myEuro = eurotherm.eurotherm2408(port)
            print("Connecte sur "+port)
            break
        except Exception as e:
            print("Pas d'equipement connecte sur "+port)



    myEuro.gui(updateTime=1,lightmemory=True)
