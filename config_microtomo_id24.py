import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
qapp = qt.QApplication([])

keepgoing = True

def printTemp(myEuro):
    print ("Temperature via callback {temp}".format(temp=myEuro.temperature)) 


def rampTest(myEuro,updateThread, startingTemp = 170 , maxTemp = 550, step = 25, maxTime = 15*60):
    global keepgoing
    
    while(keepgoing):
        myEuro.setpoint = startingTemp - 10
        starttime=time.time()
        tempstart = myEuro.temperature
        while myEuro.temperature  > startingTemp:
            rate = (myEuro.temperature - startingTemp)/((time.time()-starttime)/60.0)
            print("Cooling", startingTemp, str(myEuro.temperature), str(rate))
            #updateThread.pidPlot("Cooling at {rate}deg/min".format(rate=rate))
            time.sleep(10)
        for s in range(startingTemp, maxTemp, step):
                starttime=time.time()
                myEuro.setpoint = s
                time.sleep(maxTime/3.0)
                updateThread.pidPlot("T={temp} degC".format(temp="%.3f"%myEuro.temperature))
                time.sleep(maxTime/3.0)
                updateThread.pidPlot("T={temp} degC".format(temp="%.3f"%myEuro.temperature))
                time.sleep(maxTime/3.0)
                updateThread.pidPlot("T={temp} degC".format(temp="%.3f"%myEuro.temperature))
 

def findP(myEuro, updateThread, maxTime=60, startingP = 1450, endingP = 100, stepP=-50):

    global keepgoing
    try:
        #myEuro.Ramp_Rate_Disable = 1
        
        #myEuro.setpoint = targetTemp
            
        for p in range(startingP, endingP, stepP):
            if not keepgoing: break
            print("P", p)
            myEuro.P = p
            updateThread.pidPlot()
            time.sleep(maxTime)
    except Exception as e :
        print("findP crashed",str(e))

    
def findD(myEuro, updateThread, maxTime=600, startingD = 0, endingD = 200, stepD=10, targetTemp=100 ):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
        myEuro.setpoint = targetTemp
            
        for d in range(startingD, endingD, stepD):
            if not keepgoing: break
            print("D", d)
            myEuro.D = d
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid)
        updateThread.pidPlot()
        print("Found d ! ",str(myEuro.pid))
    except Exception as e :
        print("findP crashed",str(e))

    
def findI(myEuro, updateThread, maxTime=600, startingI = 0, endingI = 200, stepI=10, targetTemp=100 ):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
        myEuro.setpoint = targetTemp
            
        for i in range(startingI, endingI, stepI):
            if not keepgoing: break
            print("I", i)
            myEuro.I = i
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid)
        updateThread.pidPlot()
        print("Found i ! ",str(myEuro.pid))
    except Exception as e :
        print("findI crashed",str(e))



        
def shakeAndFindP(myEuro, updateThread, maxTime=600, startingP = 1450, endingP = 100, stepP=-50, stepTemp=5):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 0
        
            
        for p in range(startingP, endingP, stepP):
            myEuro.setpoint = myEuro.setpoint + stepTemp
            
            if not keepgoing: break
            print("P", p)
            myEuro.P = p
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid, myEuro.setpoint)
        updateThread.pidPlot()
        print("Found p ! ",str(myEuro.pid))
    except Exception as e :
        print("findP crashed",str(e))

    
def autotuneRamp(myEuro, updateThread, startingTemp = 50, endingTemp = 500, step=50):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
            
        for temp in range( startingTemp , endingTemp , step):
            myEuro.setpoint = temp
            
            if not keepgoing: break

            print("Autotune start for {temp}".format(temp=temp))
            myEuro.Autotune_enable = 1

            time.sleep(10)
            auto = 1
            while auto and keepgoing:
                try:
                    auto = myEuro.Autotune_enable
                    if auto == 0:
                        time.sleep(5)
                        auto = myEuro.Autotune_enable
                except Exception as e:
                    print("Pbl while reading autotune parameters",str(e))
                time.sleep(1)

            updateThread.pidPlot()        
            print("Autotune done for {temp}, pid={pid}".format(temp=temp, pid=str(myEuro.pid)))

            time.sleep(60)

            
    except Exception as e :
        print("Autune ramp crashed",str(e))

def manualTempVsTension(myEuro, updateThread):    
    # Definition of the tension vs max temperature:
    
    myEuro.manual = True
    myEuro.power = 0

    updateThread.updateTime=5
    
    while myEuro.temperature > 40.0:
        time.sleep(10)
        print("Cooling to 40 degC", myEuro.temperature)

    updateThread.updateTime=0.5

    for power in range(0,99,1):
        myEuro.power = power/10.0
        time.sleep(60)
        print("power output vs temperature", power/10.0, myEuro.temperature)
        updateThread.pidPlot(text=str(power/10.0))
        

    
    myEuro.power = 0

    
    
if __name__ == "__main__":

    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    myEuro.Adaptive_tune_enable=0
    myEuro.Autotune_enable = 0




    global app
    app = qt.QApplication([])


    mySilxEurotherm = eurotherm.silxEurotherm(myEuro)
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    updateThread = eurotherm.UpdateThread(mySilxEurotherm, updateTime=0.1)

    # open silx window
    mySilxEurotherm.show()
    
    updateThread.start()  # Start updating the plot

    time.sleep(2)
    updateThread.pidPlot()

    print(myEuro.temperature)

    myEuro.rampRate = 5
    
    
    # HWL Microtomo .
    # maxRetran = 89
    # Tension hard limit = 9.4V
    # Current max 11 A

    # Trolley : (700, 278, 46)

#    myEuro.pid = (342, 30, 5)
#    myEuro.rampRate = 30
    # myEuro.setpoint = 200

    
    #myEuro.pid = (366, 452, 75) # Email sent to Kirill. Slow to reach temp, but stable afterward.

   
    # 
    
    myEuro.pid =(60, 400, 40)
    # rampTest(myEuro,updateThread, startingTemp = 170 , maxTemp = 550, step = 25, maxTime = 15*60):
    ramp = threading.Thread(target=rampTest, args=(myEuro,updateThread, 230, 550, 25, 5*60))
    ramp.start()
    print("ramp.join() ;keepgoing=False  ; # To stop it  ")

    # myEuro.pid = (700, 0, 0) # 
    # ramp = threading.Thread(target=findP, args=(myEuro, updateThread, 60, 300, 100, -25))
    # ramp.start()
    # print("ramp.join() ;keepgoing=False  ; # To stop it  ")

    # Auto 50 degC:
    # myEuro.pid = (422, 937, 156)

    # Auto 200 deg (324,30,5)

    # Images :
# -rw-r--r--  1 watier watier 222K Oct 24 08:26 microtomo_id24_vacuum_366-452-75.png
# -rw-r--r--  1 watier watier 8.3M Oct 24 08:27 microtomo_id24_vacuum_366-452-75.dat
# -rw-r--r--  1 watier watier 147K Oct 24 08:28 microtomo_id24_vacuum_366-452-75_2.png
# -rw-r--r--  1 watier watier  68K Oct 24 08:29 microtomo_id24_vacuum_366-452-75_3_over-night.png
# drwxr-xr-x 33 watier watier 4.0K Oct 24 08:29 .
# -rw-r--r--  1 watier watier  56K Oct 24 08:29 microtomo_id24_vacuum_366-452-75_3_undershoot.png
