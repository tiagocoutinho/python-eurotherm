import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")


    ## New lollipop heater in the 'insitu 1200 furnace'
    # Design by Carlos
    # a stailess steel M6 nut is embedded within a ceramic paste heater
    # Sample is clamp between two hollow screws which let the beam in and out.
    # Design to do transmission
    # Version in Tungsten
    # 
    # 15V 12.1A 961C 
    # 16V 12.6A 997C
    # 
    
    # tension range 0,4 => 7.4V
    # myEuro.tensionRange = (0,8) => 13V
    # myEuro.tensionRange = (0,9) => 14V
    # myEuro.tensionRange = (0,10) => 15V
    
    # GOOD :
    # myEuro.tensionRange = (0,12) 

    myEuro.rampRate = 20

    # End of ramp is funny, but otherwise ok <400 
    #myEuro.pid = (30, 300, 0)
    
    # GOOD : 
    # Overshoot then stable within +/- 0.15 C
    myEuro.pid = (500, 50, 10)
    
