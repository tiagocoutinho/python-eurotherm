#!/usr/bin/env python
# -*- coding: utf-8 -*-
import eurotherm
import minimalmodbus
import time
import threading
import math

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])




class silxEurotherm(Plot1D):
    
    _sigAddCurve = qt.Signal(tuple, dict)
    """Signal used to perform addCurve in the main thread.

    It takes args and kwargs as arguments.
    """

    def __init__(self, myEuro, myEuro2, parent=None):

        super(silxEurotherm, self).__init__(parent)
        # Connect the signal to the method actually calling addCurve
        self._sigAddCurve.connect(self.__addCurve)

        self.myEuro = myEuro
        self.myEuro2 = myEuro2
        
        self.mutex = threading.Lock()

    def __addCurve(self, args, kwargs):
        """Private method calling addCurve from _sigAddCurve"""
        self.addCurve(*args, **kwargs)

    def addCurveThreadSafe(self, *args, **kwargs):
        """Thread-safe version of :meth:`silx.gui.plot.Plot.addCurve`

        This method takes the same arguments as Plot.addCurve.

        WARNING: This method does not return a value as opposed to Plot.addCurve
        """
        self._sigAddCurve.emit(args, kwargs)

class UpdateThread(threading.Thread):
    """Thread updating the curve of a :class:`keithley_2510_temperature`

    :param plot1d: The ThreadSafePlot1D to update."""

    def __init__(self, plot1d, updateTime=1):
        self.plot1d = plot1d
        self.running = False
        self.updateTime = updateTime
        self.debugPrint = False
        super(UpdateThread, self).__init__()

    def start(self):
        """Start the update thread"""
        self.running = True
        super(UpdateThread, self).start()

    def run(self):
        """Method implementing thread loop that updates the plot"""
        startingTime = time.time()
        self.history = []
        self.temperatures = []
        self.temperatures2 = []
        self.targetSP = []
        
        self.resetLock = threading.Lock()
        
        while self.running:
            time.sleep(self.updateTime)
            with self.resetLock :
                try:                    
                    temp = float(self.plot1d.myEuro.temperature)
                    temp2 = float(self.plot1d.myEuro2.temperature)
                    curset = float(self.plot1d.myEuro.workingSetpoint)
                    
                    if( type(temp) == float and type(temp2) == float and type(curset) == float ):


                        self.history.append( time.time()-startingTime )
                        self.temperatures.append(temp)
                        self.temperatures2.append(temp2)
                        self.targetSP.append(curset)


                    if (len(self.history) == len(self.temperatures) ):
                        self.plot1d.addCurveThreadSafe(
                            x=numpy.asarray(self.history), y=numpy.asarray(self.temperatures), resetzoom=True, legend="Temperature", linewidth=4, yaxis="left")
                        self.plot1d.addCurveThreadSafe(
                            x=numpy.asarray(self.history), y=numpy.asarray(self.temperatures2), resetzoom=True, legend="Temperature2", linewidth=4, yaxis="left")
                        self.plot1d.addCurveThreadSafe(
                            x=numpy.asarray(self.history), y=numpy.asarray(self.targetSP), resetzoom=True, legend="Target", linewidth=1.5, linestyle=":", yaxis="left")
                        
                    else:
                        if self.debugPrint : print("Error on length", len(self.history), len(self.temperatures))

                except Exception as e:
                    if self.debugPrint: print(str(e))
                    
    def stop(self):
        """Stop the update thread"""
        self.running = False
        self.join(2)

    def reset(self):
        with self.resetLock :
            self.history = []
            self.temperatures = []
            self.temperatures2 = []
            self.targetSP = []
        
        


    def pidPlot(self, text=None):

        if(text==None):text=str(self.plot1d.myEuro.pid)
        
        #with self.resetLock :
        self.plot1d.addMarker(
                x=self.history[-1],
                y=self.temperatures[-1],
                text=text,
                draggable=True
                )

keepgoing = True
    
def gui(updateTime=1):
    
    global app
    app = qt.QApplication([])

    time.sleep(1)
    global mySilxEurotherm
    mySilxEurotherm = silxEurotherm(myEuro, myEuro2)
    time.sleep(1)
    
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    global updateThread
    updateThread = UpdateThread(mySilxEurotherm, updateTime=updateTime)

    time.sleep(1)
    # open silx window
    mySilxEurotherm.show()
    time.sleep(1)
    
    updateThread.start()  # Start updating the plot

    time.sleep(1)
    
    updateThread.pidPlot()

    print(myEuro.temperature)



if __name__ == '__main__':


    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")

    global myEuro2
    myEuro2 = eurotherm.eurotherm2408("/dev/ttyUSB2")
