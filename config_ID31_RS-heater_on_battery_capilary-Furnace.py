import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")
    #myEuro.Adaptive_tune_enable=0
    #myEuro.Autotune_enable = 0


    # 3x200W 240V RS heater in parallel mounted in the copper plate for battery capillary on ID31 (2 places)
    
    myEuro.tensionRange = (0, 50)  ## Need max power as it is a 3x240V heater from RS.
    
    # @50C autotune :  myEuro.pid = (5, 165, 27)
    # A la mano @80C:
    #myEuro.pid = (1,72,0)   #35-40%
    #myEuro.rampRate = 10

    # 80 => 200C @10C/min => OK with (1,72,0) but oscilate period 19
    
    # myEuro.Output_rate_limit = 2000
    # myEuro.rampUnit = 'Minutes'
    # myEuro.rampRate = 30
    
    #myEuro.pid = (1, 19, 0)

    # @50C, no isolation, themocouple K, stability is +/-40mK with small spikes at +/-60mK
    # @80C 100mK stability
    

    # Ramprate = 10 good.

    # test ramp 50C = > 80C @ 15C/min (1, 19, 0)
    # ramp OK overshoot of 750mK  (no D)
    
    # Picture :   ID31_battery_capilary_ramp_15C-min_50-to-80C_001.png


    # Ramprate = 30  90=>200C too large ramp rate.


    # GOOD : 
    myEuro.pid = (1, 19, 0)
    myEuro.rampRate = 10
    


    # @80C:
    # Thermocouple for regulation
    # pt100 inside the capillary 7deg Offset @80C
    # Sample  Setpoint
    # 80      88
    # 100     108
    
    
