import json
from flask import Flask, jsonify, render_template, request

import eurotherm

app = Flask(__name__)

myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")
    

def listeTemperatures(listeHosts, page="live", selectionSondes=None):
    
    temperatures=getTemperatures(listeHosts)
    
    html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"       "http://www.w3.org/TR/html4/frameset.dtd"><HTML>'

    html += '<head><meta http-equiv="refresh" content="1; url=/{live}"><TITLE>Temperatures</TITLE></head>'.format(live=page)

    html+=dictTempToHtml(temperatures, selectionSondes=selectionSondes)

    return html



@app.route('/reconnect')
def reconnect():
    myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")
    return ""

@app.route('/getvalue/<string:param>')
def getvalue(param):
    print(param, myEuro.__getattr__(param))
    return jsonify(param=param, value=str(myEuro.__getattr__(param)))

@app.route('/setvalue/<string:param>/<string:value>')
def setvalue(param, value):
    myEuro.__setattr__(param,value)
    return getvalue(param)

@app.route('/set.php', methods=['POST'])
def set():
    res = ""
    for key in request.form.keys():
        #print("In set.php, with" +key)
        res += str(setvalue(key, request.form[key]))

    res = "<script>window.history.back();</script>"

    return res 



    

@app.route('/pid')
def pid():
    params=['Proportional_band_PID1',
            'Integral_time_PID1',
            'Derivative_time_PID1',
            'Working_set_point',
            'Target_setpoint',
            'Process_Variable',
            'Local_or_remote_setpoint_select',
            'Ramp_Rate_Disable',
            'Setpoint_rate_limit'
        ]
    return tableauParams(params, refresh=10, titre="PID")
    

@app.route('/ramp')
def ramp():
    params=['Working_set_point',
            'Target_setpoint',
            'Process_Variable',
            'Local_or_remote_setpoint_select',
            'Ramp_Rate_Disable',
            'Setpoint_rate_limit',
            'Ramp_rate',
            'rampRate'
        ]
    return tableauParams(params, refresh=1, titre="Ramp")
    

@app.route('/temperature')
def temperature():
    params=[
            'Working_set_point',
            'Target_setpoint',
            'Process_Variable',
        ]
    return tableauParams(params, refresh=1, titre="Temperatures")
    

@app.route('/all')
def all():
    return tableauParams(myEuro._registers.keys(), refresh=600, titre="One page to rule them all.")
    

def tableauParams(params, refresh=10, titre="Live"):
    html =   """
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

    <script>
function getvalueparam(param) {
  $.getJSON('/getvalue/' + param ,
     function(data){    
       console.log( "param : "+data.value );
       $('#value_'+param).text(data.value);
     }
    )
}
    </script>
</head> 
<body>
"""
    html+="""
<h1>{titre}</h1>
""".format(titre=titre)

    html += '<table border="1">'
    
    
    for param in params:
        html += """
    <tr>
      <th>
        <p id="nom_{param}"> {param} </p>
      </th>
      <th>
        <p id="value_{param}"> Non synchronise </p>
      </th>
      <th>
        <p id="edit_{param}">
                 <form action="set.php" method="post"  data-rel="back"> 
                    Change for : <input type="text" name="{param}" value=""> 
                    <input type="submit"> 
                 </form> 
        </p>
      </th>
    </tr>

    <script>
    getvalueparam("{param}");
    setInterval('getvalueparam("{param}")', {refresh});
    </script>
    """.format(param=param, refresh=refresh*1000 )


            

    html += '</table>'


        
    html += """
</body>
</html>
    """
    return html



    
@app.route('/sensor')
def sensor():
    html =   """
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

    <script>
function getvalueparam(param) {
  $.getJSON('/getvalue/' + param ,
     function(data){    
       console.log( "param : "+data.value );
       $('#value_'+param).text(data.value);
     }
    )
}
    </script>
</head> 
<body>
"""
    html+="""
<h1>Sensor: {sensor}</h1>
""".format(sensor=myEuro.temperatureSensor)

    html += '<table border="1">'
    
    
    for sensor in ["J","K", "L", "R", "B", "N", "T", "S", "Pt100", "0-10V", "Linear mA"]:
                   
        html += """
    <tr>
      <th>
        <p id="nom_{sensor}"> {sensor} </p>
      </th>

      <th>
        <button type="submit" onclick="window.location='setvalue/temperatureSensor/{sensor}'"> Change eurotherm sensor for : {sensor} </button>
        Warning take 5-15 sec to reboot eurotherm.
      </th>
    </tr>

""".format(sensor=sensor)

        
    html += """
 '</table>'
</body>
</html>
    """
    return html




@app.route('/')
def index():
    html =   """
<!DOCTYPE html>
<html>
<head>
<a href=temperature>Current temperature and setpoint</a> </br> </br> 

<a href=ramp>Ramp rate change</a> </br> </br> 

<a href=pid>PID change</a> </br> </br> 

<a href=sensor>Sensor type change</a> </br></br>  

</html>

"""
    return html
    
if __name__ == '__main__' :
    
    try:
        app.run(debug=True, host='0.0.0.0', port=8080, threaded=True)
    except Exception as e:
        print("Could not connect to port "+str(port)+"\n"+str(e))
        
            

    
