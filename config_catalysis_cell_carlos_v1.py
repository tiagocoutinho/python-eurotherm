import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")

    # Initial round of experiment with Rafal on ID26
    # Aluminium cell with 0.7mm kanthal resistance
    # Chiller

    # Cell better known as 'Quetalysis cell'
    
    myEuro.tensionRange = (0, 6) 

    myEuro.Output_rate_limit = 2000


    # Carlos initial pid 180 50 0
    # Carlos ramp 8/min


    # possible cooling rate (350 -> 200) - 120C / min
    #

    # slow but ok:
    # myEuro.pid = (50,40,20)

    # Good ones:
    # At 250C +/-0.05 C
    
    myEuro.pid = (48,37,6)
    myEuro.rampRate = 100
    
