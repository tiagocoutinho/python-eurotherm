import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
def gui():
    
    global app
    app = qt.QApplication([])

    time.sleep(1)
    global mySilxEurotherm
    mySilxEurotherm = eurotherm.silxEurotherm(myEuro)
    time.sleep(1)
    
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    global updateThread
    updateThread = eurotherm.UpdateThread(mySilxEurotherm, updateTime=0.1)

    time.sleep(1)
    # open silx window
    mySilxEurotherm.show()
    time.sleep(1)
    
    updateThread.start()  # Start updating the plot

    time.sleep(1)
    
    updateThread.pidPlot()

    print(myEuro.temperature)


if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    myEuro.Adaptive_tune_enable=0
    myEuro.Autotune_enable = 0

    
    #myEuro.temperatureSensor = 'K'
    
    #gui()

    # configuration for Novitom on ID19.
    #  setup is a traction machine with traction plate in stainless steel
    # For first test we put a thermocouple clamped in the machine.
    # Next will be with different plates and a hole for the thermocouple.

    # Previous use of the induction furnace on ID31:
    # myEuro.pid = (96, 26, 4)
    # myEuro.Output_rate_limit = 20    
    # myEuro.tensionRange = (43,49)
    #      => 7% manuel =~ 250degC entre les mors.
    #      => 8% manual =~ 125kHz 300degC

    # 43,49 Not enough limitation, 43,44.
    # myEuro.tensionRange = (43,44)
    #     => 50% 285C easily.
    #     => 90% 300C easily

    # Test @200C 96,26,4  rateLimit 20
    # Going down ther is an undershoot 3.5 (from 300 to 200) regulation is within 1 degC
    # 200 degC =~ 14% 
    # test autotune
    # pid = (7,19,3)
    
    # @200deg C +/-150mK : pid =(20, 19, 3) Output_rate_limit = 200
    # ramprate 20deg/min : OK (between 200 and 300 degC )
    # 3 deg overshoot (rate limit too low ?)
    # Then stable

    # Second test with something that can be pressed between the plate of the press.
    # myEuro.Output_rate_limit = 800

    # Second round of test with users + novitom + samples.
    # Need more juice, as the press need to heated opened in half
    
    # myEuro.tensionRange = (43, 46) # Does the job, but can benifit from more to follow a ramp properly. To be seen on ID19.
    
