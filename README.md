EuroTools : 

euroTools.py -h
usage: euroTools.py [-h] [-v] [-d] [-s SAVE_IN_FILE] [-c COMPARE_WITH_FILE]
                    [-f FLASH_WITH_FILE]
                    serial_line

Use eurotherm on a modbus serial line.

positional arguments:
  serial_line           Which serial line is the eurotherm connected on,
                        something like /dev/tty? /dev/ttyRP2 ...

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Print requests on the serial line
  -d, --dump            Print all parameters from eurotherm
  -s SAVE_IN_FILE, --save-in-file SAVE_IN_FILE
                        Save eurotherm configuration in a specific file
  -c COMPARE_WITH_FILE, --compare-with-file COMPARE_WITH_FILE
                        Compare ther eurotherm configuration with a specific
                        file
  -f FLASH_WITH_FILE, --flash-with-file FLASH_WITH_FILE
                        flash the eurotherm configuration with a specific file





Eurotherm.py :
A class to use eurotherms through modbus serial lines.
2400 series: OK

On a 2408, one can access (R/W)  356 parameters. Names are based on the eurotherm official manual for convenience.


Parameters tested on a 2408 so far :
['Maximum_Input_Value',
 'Cold_junction_compensation',
 'Input_value_low',
 'Dwell_Segment',
 'Alarm_4_Blocking',
 'Working_set_point__ReadOnly__',
 '2A_Sense_of_output',
 'Servo',
 'Segment_time_remaining_in_mins',
 'DI2_Low_scalar',
 'Alarm_2_Latching_',
 '3C_Module_3C_function',
 'Remote_low_power_limit',
 'Power_in_end_segment',
 'Configuration_of_lower_readout_display',
 'Maximum_Control_Task_Time__Processor_utilisation_factor_',
 'Fast_run',
 'setpoint16',
 'setpoint15',
 'setpoint14',
 'setpoint13',
 'Sensor_break_output_power',
 'VP_Bounded_sensor_break',
 'setpoint10',
 '4A_Module_identity',
 '1A_Maximum_electrical_output',
 'Setpoint_rate_limit',
 'Front_panel_Auto/Manual_button',
 'Setpoint_Max.__High_range_limit_',
 'Front_panel_Run/Hold_button',
 'Instrument_units',
 'Derivative_action',
 'Display_value_corresponding_to_input_8',
 '2C_Summary_of_2C_configuration',
 'Display_value_corresponding_to_input_2',
 'Display_value_corresponding_to_input_3',
 'Display_value_corresponding_to_input_1',
 'Display_value_corresponding_to_input_6',
 'Display_value_corresponding_to_input_7',
 'Display_value_corresponding_to_input_4',
 'Display_value_corresponding_to_input_5',
 'PV_mean_value',
 'Alarm_1_type',
 'Type_of_cooling',
 'Alarm_3_Latching_',
 'CNOMO_Manufacturers_ID',
 'Holdback_Disable',
 'Gain_scheduler_setpoint',
 '4A_Module_function',
 'Local_setpoint_trim_high_limit',
 '1C_Module_1C_value_giving_min_output',
 'AA_Sense_of_output',
 'Auto-man_select',
 '1C_Summary_of_1C_configuration',
 'Cool_hysteresis__on/off_output_',
 '3C_Sense_of_output__nor/inv_as_3A_',
 '3A_Potentiometer_input_3A_high_scalar',
 'Programmable_event_outputs',
 'Heat_cycle_time',
 'Feedforward_proportional_band',
 'Input_2_cold_junction_temp._reading',
 'Displayed_reading_low',
 'Manual_reset_PID1',
 'Manual_reset_PID2',
 'Programmer_State_Write',
 'Remote_Parameter',
 'Enable_diagnostic_messages',
 'PV_maximum',
 'Program_cycles_remaining',
 'Loop_break_time',
 'AA_Retransmitted_Low_Scalar',
 '1C_Summary_program_O/P_1C_config.',
 'Setpoint_Span',
 '2A_PID_or_Retran_low_value',
 '3A_Units_3A',
 'Ratio_setpoint',
 'Advance_Segment_Flag',
 'Target_setpoint__current_segment_',
 'Holdback_value_for_srtpoint_rate_limit',
 'Alarm_4_type',
 'Power_feedback_enable',
 'Automatic_droop_compensation__manual_reset_',
 '2A_Summary_output_2A_configuration',
 'B_Valve_Posn__computed_by_VP_algorithm_',
 'BBB_Alarm_1setpoint_value',
 '2C_Module_2C_identity',
 'Heater_current__With_PDSIO_mode_2_',
 '4A_Input_module_4A_low_value',
 '3B_Sense_of_output__nor/inv_as_3A_',
 'Cool_output_minimum_on_time',
 'Local_setpoint_trim_low_limit',
 'Setpoint_2_low_limit',
 'Error__PV-SP_',
 'AA_Retransmitted_High_Scalar',
 'Heat_output_minimum_on_time',
 'Input_2_filter_time_constant',
 'Setpoint_2_high_limit',
 'Segment_time_remaining_in_secs',
 'Derived_input_function_factor_1',
 'AA_Summary_of_AA_configuration',
 'Selected_calibration_point',
 'Potentiometer_Calibration_Enable',
 '2B_Module_2B_identity',
 '2C_Sense_of_output__nor/inv_as_2A_',
 'Low_power_limit',
 'Setpoint_1_high_limit',
 'Current_program_running__active_prog_no._',
 '3A_Potentiometer_input_3A_low_scalar',
 'Setpoint_1_low_limit',
 'Switchover_transition_region_high',
 'setpoint12',
 'setpoint11',
 'PV_minimum',
 'Integral_time_PID2',
 'Alarm_1_hysteresis',
 'Autotune_enable',
 'Cutback_low_PID1',
 'Cutback_low_PID2',
 'PV_threshold_for_timer_log',
 'VP_motor_calibration_state',
 'User_calibration_enable',
 'Program_status_:',
 'Logic_6_output__current_program_',
 'Configuration_Level_Password',
 '3A_Module_3A_low_value',
 '2A_Maximum_electrical_output',
 'DI1_Logic',
 'Working_output',
 'PDSIO_SSR_status',
 'All_User_Interface_Keys_Disable',
 '3B_Module_3B_identity',
 'Potentiometer_Calibration_Go',
 '1C_Module_1C_identity',
 'Feed_forward_type',
 '4A_Maximum_electrical_output',
 '2A_PID_or_Retran_high_value',
 'Cutback_high_PID2',
 'BBB_Alarm_2setpoint_value',
 'Output_rate_limit',
 '2B_Summary_program_O/P_2B_config.',
 'Minimum_Input_Value',
 'Emmisivity_input_2',
 'Input_1_filter_time_constant',
 '2A_Units',
 'Alarm_3_hysteresis',
 'Remote_setpoint',
 '3A_Sensor_break_impedance__input_2',
 'Bounded_sensor_break_strategy',
 '3A_Module_identity',
 '%_Output_power',
 'Ramp_rate',
 'Current_segment_type',
 'Programmer_tracking',
 'Derivative_component_of_output',
 'Logic_4_output__current_program_',
 '4A_Minimum_electrical_output',
 'BCD_Input_Value',
 'Remote_tracking',
 '2C_Module_2C_function',
 'Program_time_remaining',
 '3A_Maximum_electrical_output',
 '1C_Module_1C_Minimum_electrical_output',
 'Logic_8_output__current_program_',
 'Input_2_calibration_offset',
 '2C_Summary_program_O/P_2C_config.',
 'Slave_Instrument_Ramp_Rate',
 'Remote_SRL_Hold',
 'Manual_tracking',
 'DC_Output_1A_Telemetry',
 '2A_Program_summary_output_2A_conf.',
 '1A_Sense_of_output',
 'Integral_component_of_output',
 'Derivative_time_PID2',
 '1A_Module_identity',
 'Remote_Input_Value',
 'Displayed_reading_high',
 'Switchover_transition_region_low',
 'Remote_setpoint_trim',
 'AA_Module_function',
 'Alarm_3_type',
 'On/Off_Sensor_Break_Output',
 '4A_Program_summary_output_4A_config',
 'Access_Mode_Password',
 'DI2_Input_functions',
 'Alarm_3_Blocking_',
 '2B_Sense_of_output__nor/inv_as_2A_',
 'Logic_5_output__current_program_',
 'Alarm_4_hysteresis',
 'Logic_1_output__current_program_',
 '3A_Input_module_3A_low_value',
 'Alarm_2_Blocking_',
 'A._Segment_synchronisation',
 'Input_1_cold_junction_temp._reading',
 '3B_Summary_of_3B_configuration',
 'Flash_active_segment_in_lower_display',
 'Gain_schedule_enable',
 'Customer_defined_identification_number',
 'DI2_High_scalar',
 'Instrument_Version_Number',
 'Adaptive_tune_enable',
 'Feedforward_component_of_output',
 'Alarm_4_Latching',
 'Minimum_pulse_time',
 'Sensor_break_impedance',
 'Heat_hysteresis__on/off_output_',
 'setpoint9',
 'setpoint8',
 'setpoint7',
 'setpoint6',
 'setpoint5',
 'setpoint4',
 'setpoint3',
 'setpoint2',
 'setpoint1',
 'Setpoint_rate_limit_units',
 'Program_Logic_Status',
 '1A_PID_or_Retran_value_giving_max._o/p',
 'User_calibration_adjust_input_1',
 'Local_setpoint_trim',
 'User_calibration_adjust_input_2',
 'Input_value_high',
 'Feedforward_trim_limit',
 '2A_Module_identity',
 '3A_Cold_junction_compensation__ip_2',
 '3C_Summary_program_O/P_3C_config.',
 'Programmer_type',
 '3C_Module_3C_identity',
 '2A_Potentiometer_input_high_scalar',
 'Programmer_setpoint',
 'Input_2_measured_value',
 'Current_PID_set',
 'Integral_and_Derivative_time_units',
 'Alarm_1_Blocking',
 '1C_Module_1C_function',
 'Input_type',
 'AA_Module_Identity',
 '4A_Summary_output_4A_configuration',
 '1B_Module_1B_function',
 'Slave_Instrument_Target_Setpoint',
 '4A_Input_module_4A_high_value',
 'Process_Variable',
 'Programmer_state_Read',
 'Input_1_linearised_value',
 'Acknowledge_All_Alarms',
 'Goto',
 'Control_type',
 'AA_Module_identity',
 '1B_Summary_program_O/P_1B_config.',
 '3A_Sense_of_output',
 '3A_Minimum_electrical_output',
 '2A_Potentiometer_input_low_scalar',
 'DI1_Input_functions',
 '3A_Input_value_low',
 'B_VP_Manual_Output__alterable_in_Man_only_',
 '3B_Summary_program_O/P_3B_config.',
 'Integral_time_PID1',
 '2B_Summary_of_2B_configuration',
 'Valve_inertia_time',
 'Potentiometer_Input_Calibration_Node',
 'BBB_Alarm_4setpoint_value',
 'Derivative_time_PID1',
 '3A_Summary_output_3A_configuration',
 'Remote_high_power_limit',
 '1B_Sense_of_output__nor/inv_as1A_',
 'Logging_reset',
 '3A_Input_module_3A_high_value',
 'Currently_selected_setpoint',
 'BCD_input_function',
 'Number_of_setpoints',
 'Select_input_1_or_input_2',
 'Alarm_2_hysteresis',
 'AA_Comms_Resolution',
 '1A_Units',
 '1A_Program_summary_output_1A_config',
 'Alarm_1_Latching',
 'Remote_setpoint_configuration',
 '1A_Module_function',
 'Instrument_Mode',
 'Setpoint_Min.__Low_range_limit_',
 '3A_Program_summary_output_3A_config',
 'Proportional_component_of_output',
 'Logic_2_output__current_program_',
 'Time_PV_above_threshold_level',
 '3A_Module_3A_high_value',
 '5D6_Sensor_break_output',
 'Pot_Position',
 'Ramp_Rate_Disable',
 'Adaptive_tune_trigger_level',
 'Logic_7_output__current_program_',
 '4A_Sense_of_output__nor/inv_as_3A',
 '2B_Module_2B_function',
 'DC_Output_3A_Telemetry',
 'Cool_cycle_time',
 'Valve travel_time',
 '3A_Input_value_high',
 'Synchronisation_of_programs',
 'Current_segment_number',
 'Input_1_calibration_offset',
 'High_power_limit',
 'Valve_backlash_time',
 '3B_Module_3B_function',
 'BBB_Alarm_3setpoint_value',
 'Power_fail_recovery',
 '2A_Minimum_electrical_output',
 'Cutback_high_PID1',
 '1A_PID_or_Retran_value_giving_min._o/p',
 '1C_Sense_of_output__nor/inv_as_1A_',
 '3C_Summary_of_3C_configuration',
 '1A_Minimum_electrical_output',
 'Alarm_2_type',
 'Target_setpoint',
 '1A_DC_output_1A_telemetry_parameter',
 'Instrument_Ident',
 'Forced_manual_output',
 'Error_Logged_Flag',
 'Slave_Instrument_Sync',
 '3A_input_type__input_2',
 'Logic_3_output__current_program_',
 '3A_Module_function',
 'AA_Program_summary_OP_AA_configuration',
 'Holdback_type_for_sp_rate_limit',
 'Holdback',
 'Control_Action',
 '1B_Summary_of_1B_configuration',
 'selectedSetpoint',
 'Input_2_linearised_value',
 'Skip_Segment_Flag',
 '1B_Module_1B_identity',
 'Proportional_band_PID2',
 'Proportional_band_PID1',
 'DC_Output_2A_Telemetry',
 'Feedforward_trim',
 '1C_Module_1C_value_giving_max_output',
 'Decimal_places_in_displayed_value',
 'Input_1_measured_value',
 'Manual/Auto_transfer_PD_control',
 'Maximum_Number_Of_Segments',
 'Relative_cool_gain_PID2',
 'Relative_cool_gain_PID1',
 '2A_Module_function',
 'Forced_output_level',
 '_Derived_input_function_factor_2',
 'Emmisivity',
 'Custom_linearisation_input_8',
 'Custom_linearisation_input_7',
 'Custom_linearisation_input_5',
 'Local_or_remote_setpoint_select',
 'DI2_Identity:',
 'Heat/cool_deadband__on/off_op_',
 'Custom_linearisation_input_6',
 '1C_Module_1C_Maximum_electrical',
 'Custom_linearisation_input_4',
 '1A_Summary_output_1A_configuration',
 'Custom_linearisation_input_2',
 'Custom_linearisation_input_3',
 'Custom_linearisation_input_1']
