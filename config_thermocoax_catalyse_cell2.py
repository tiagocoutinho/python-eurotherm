import templogger
import matplotlib.pyplot as plt
import time
import eurotherm


def printTemp(myEuro):
    print ("Temperature via callback {temp}".format(temp=myEuro.temperature)) 


if __name__ == "__main__":

    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB1")
    
    # myEuro.Ramp_Rate_Disable = 1
    # time.sleep(0.5)
    # myEuro.setpoint = 20
    # time.sleep(0.5)

    # Configuration for ID26 inhouse.
    # Rafal's need :
    #     activation of sample at 450C
    #     Stability at 350C

    # Need 35V 1.8A to get 450degC (just, with a plateau to arrive there, and no gas flow in the cell... )

    if False :  # Only one time to enforce configuration of eurotherm
        myEuro.tensionRange = (0.0,5)
        myEuro.Low_power_limit = 0 
        myEuro.High_power_limit = 100

    
    # test at 350 degC
    # BG cheat sheet : (42,86,4) 
    #myEuro.pid = (42,86,4) # -4 +4 deg
    #myEuro.pid = (42,86,20) #  silly
    #myEuro.pid = (42,86,2) #  worse

    # only P :
#    myEuro.pid = (35,0,0)# +2 -11
    #myEuro.pid = (15,0,0)# -8 +2
#    myEuro.pid = (5,0,0) # -5 +3 -7 +4
#    myEuro.pid = (3,0,0) # -5 +3 -7 +4

    #myEuro.pid = (1,0,0) #  -7 +6
    # oscilation time 92.2sec

#    myEuro.pid = (1, 96*0.5, 96*0.12) #   Best so far +1 -3 50sec oscilations
    #myEuro.pid = (1, 96*0.5, 96*0.32) #   # -4 +0
    #myEuro.pid = (1, 92*0.5, 96*0.5) #
    
    #    myEuro.pid = (200,0,0) #
    #    myEuro.pid = (150,0,0) #
    #myEuro.pid = (100,0,0) #
    #myEuro.pid = (80,0,0) #

    #myEuro.pid = (500,0,0) # goes down
    #myEuro.pid = (300,0,0) # Oscilate and settle
    #myEuro.pid = (400,0,0)      #  goes down
    
    #myEuro.pid = (350,0,0) # 322-335 oscilationfor 350. time between over and undershoot 60 sec.
    ponly = 350
    tou = 60 # "Good gain method"  Time between first over and first undershooting (not the period)
#    myEuro.pid = (ponly*0.8, 1.5*tou, 0)
    myEuro.pid = (ponly*0.8, 1.5*tou, 1.5*tou/4.0)
    print(str(myEuro.pid))

    ramp = 10
    if myEuro.rampRate != ramp:
        myEuro.rampRate = ramp # 40 

    startingTemp = myEuro.temperature

    targetTemp = 30
    
    while myEuro.temperature > startingTemp:
        print(startingTemp, str(myEuro.temperature))
        time.sleep(1)
    #myEuro.Ramp_Rate_Disable = 0
    #myEuro.setpoint = startingTemp
    #myEuro.Autotune_enable = 0

    
    #time.sleep(10)
    #print(" Adaptive tuning" )
    #myEuro.Adaptive_tune_enable=0
    #myEuro.Adaptive_tune_trigger_level=0


    #time.sleep(3)
    if myEuro.setpoint != targetTemp:
        myEuro.setpoint = targetTemp
    
    mylogger = templogger.logger(myeurotherm=myEuro,sleepTime=0.3, showPower=False)

    #mylogger.callbacks.append(printTemp(myEuro))
    
    plt.show()
    
    # myEuro.Ramp_Rate_Disable = 1
    # time.sleep(0.5)
    # myEuro.setpoint = 20
    # time.sleep(0.5)
    # myEuro.Ramp_Rate_Disable = 0
    # time.sleep(0.5)

    
    print(str(myEuro.pid))

    #myEuro.Adaptive_tune_enable=0
    
    #plt.savefig(time.strftime("%Y-%m-%d_%H-%M-%S_ID24_plot.png"))
    #mylogger.saveData()
    
    #mylogger.myEuro.setpoint =20
    
