import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True


if __name__ == "__main__":
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB1")

    # Oier users for a annealing of sample in the UFO chamber.
    # Heater is Carlos's with 1mm Kanthal.
    # Top plate without the copper base.
    
    # Annealing at 350 deg 66deg/hour

    # 2.0V 1.6A => 103 degC
    # 3.0V 2.4A => 115 degC
    # 4.0V 3.2A => 200 degC
    # 5.0V 4.0A => 250 degC
    # 6.0V 4.7A => in 10 minutes 200 => 350.

    # We go for 6.0V hard limitted on the DE and 4.7A on the dial.

    

    myEuro.Output_rate_limit = 2000

    # 66 deg/hour
    myEuro.rampRate = 66.0/60.0
    
    myEuro.pid = (30,25,3)  # For 900C

    # Dash red is setpoint, dash green the working setpoint (ramp). solid blue is the thermocouple.
    # recommended pids that are a litlle bit slow but work fine:
