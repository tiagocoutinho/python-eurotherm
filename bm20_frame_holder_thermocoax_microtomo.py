import time
import eurotherm

if __name__ == "__main__":

    global myEuro
    #myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")


    # Last minute experimient on BM20, quick setup of the microtomo furnace with the square frame which will hold a sealed quartz capillary.
    # Request 800C on heater for a 650 expected temperature on the sample
    # No Vacuum
    
    
    # myEuro.Output_rate_limit = 2000
    # myEuro.tensionRange = (0, 20) # 30V => 620 C in air with cooling of the widows.
    # myEuro.tensionRange = (0, 25) +31.5V hardlimit => 650C
    # myEuro.tensionRange = (0, 25) +34.0V hardlimit => 690C
    # myEuro.tensionRange = (0, 30) +40.0V 1.3A hardlimit => 770C
    # myEuro.tensionRange = (0, 30) +42.0V 1.4A hardlimit => 780C
    # myEuro.tensionRange = (0, 31) +45.0V 1.6A hardlimit => 800C
    
    
    
    
    # myEuro.pid = (250, 200, 5)
    # At 800C :
    myEuro.pid = (55, 52, 8) # 1.2C oscilation at 800
    myEuro.pid = (55, 250, 8)
