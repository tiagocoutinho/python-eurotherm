import templogger
import matplotlib.pyplot as plt
import time
import eurotherm


def printTemp(myEuro):
    print ("Temperature via callback {temp}".format(temp=myEuro.temperature)) 


if __name__ == "__main__":

#    myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    
    # myEuro.Ramp_Rate_Disable = 1
    # time.sleep(0.5)
    # myEuro.setpoint = 20
    # time.sleep(0.5)

    # Configuration for ID26 inhouse.
    # Rafal's need :
    #     activation of sample at 450C
    #     Stability at 350C

    # Need 35V 1.8A to get 450degC (just, with a plateau to arrive there, and no gas flow in the cell... )

    if False :  # Only one time to enforce configuration of eurotherm

        print(myEuro._1A_Module_identity,\
            myEuro._1A_Module_function,\
            myEuro._1A_PID_or_Retran_value_giving_min__o_p,\
            myEuro._1A_PID_or_Retran_value_giving_max__o_p,\
            myEuro._1A_Units,\
            myEuro._1A_Minimum_electrical_output,\
            myEuro._1A_Maximum_electrical_output,\
            myEuro._1A_Sense_of_output,\
            myEuro._1A_Summary_output_1A_configuration,\
            myEuro._1A_DC_output_1A_telemetry_parameter,\
            myEuro._1A_Program_summary_output_1A_config)

        # Eurotherm in manual full output.
        # Give the max tension:
        # value = 1; myEuro.Instrument_Mode = 2; myEuro._1A_PID_or_Retran_value_giving_max__o_p = value; myEuro.Instrument_Mode = 0
        # if too low (should be close to max):
        # myEuro.tensionRange = (0.0,50.0)
        # Give the min tension:
        # value = 100; myEuro.Instrument_Mode = 2; myEuro._1A_PID_or_Retran_value_giving_max__o_p = value; myEuro.Instrument_Mode = 0
        

        
        desiredMaxTension = 7.3 # V
        onePcOoutput = 71.7 # 72.2      # V
        hundredPcOutput = 6.9    # V
        
        guess = int( (onePcOoutput - desiredMaxTension) * (onePcOoutput - hundredPcOutput)/99.0) - 2
        myEuro.Instrument_Mode = 2
        myEuro._1A_PID_or_Retran_value_giving_min__o_p = 0
        #myEuro._1A_PID_or_Retran_value_giving_max__o_p = guess
        myEuro._1A_PID_or_Retran_value_giving_max__o_p = 90  # 90 for microtomo in tension regulation  ('BEFORE', None, 17, 0, 90, 1, 0, 50, 0, 0, 0, 0)
        myEuro.Low_power_limit = 0
        myEuro.High_power_limit = 100
        myEuro.Remote_low_power_limit = 0
        myEuro.Remote_high_power_limit =100
        myEuro.Instrument_Mode = 0
        time.sleep(8)
        myEuro.tensionRange = (0.0,50.0) # Why 50 ?  

        
        print(myEuro._1A_Module_identity,\
            myEuro._1A_Module_function,\
            myEuro._1A_PID_or_Retran_value_giving_min__o_p,\
            myEuro._1A_PID_or_Retran_value_giving_max__o_p,\
            myEuro._1A_Units,\
            myEuro._1A_Minimum_electrical_output,\
            myEuro._1A_Maximum_electrical_output,\
            myEuro._1A_Sense_of_output,\
            myEuro._1A_Summary_output_1A_configuration,\
            myEuro._1A_DC_output_1A_telemetry_parameter,\
            myEuro._1A_Program_summary_output_1A_config)

        
    # test at 150 degC
    # 500 : Oscilate and settle  76sec tou
    # 600 : goes down slowly
    # 450 :  Oscilate
    # 400 : Oscilate
    # 500 + p + I @76sec tou:  Slow to reach setpoint (210sec) but stable
    # 100 + pi @76 vershoot + slow to stab
    # 200 +pi @ 76 17deg overshoot
    # 550 + pi @76 : Slow
    # 600 +pi ok a 150deg
    
    # test at 250 degC
    # 600+pi ok but 2 deg overshoot take 250sec to reach sp

    # test autotune p only : 260  Tou: 56/119 63
    # 208/94/23 : bof bof

    #Best today :(480, 114, 28) No overshoot, takes time to arrive at sp. Long term stability ok.


    # in vacuum, 100degC
    # p=30

    # Bea gave back the trolley with pid = 600,180,3
    # Yves values were : pid = (400, 426, 106)

    # (48, 13, 3) Fast not much stable

    pidPonly = False
    ponly = 60 #600?
    tou = 16-7 # "Good gain method"  Time between first over and first undershooting (not the period)
    #    myEuro.pid = (ponly*0.8, 1.5*tou, 0)
    if pidPonly:
        myEuro.pid = (ponly,0,0)
    else:
        myEuro.pid = (ponly*0.8, 1.5*tou, 1.5*tou/4.0)

    # Tested for ID10 at 200 degC
    #myEuro.pid = (892, 316, 52)
    #myEuro.pid = (48, 30, 3)
    myEuro.pid = (48, 90, 3)

        
    print(str(myEuro.pid))

    ramp = 7 # 10
    if myEuro.rampRate != ramp:
        myEuro.rampRate = ramp # 40 

    startingTemp = 100

    targetTemp = 220
    
    # while myEuro.temperature > startingTemp:
    #     print(startingTemp, str(myEuro.temperature))
    #     time.sleep(1)
    #myEuro.Ramp_Rate_Disable = 0
    #myEuro.setpoint = startingTemp
    #myEuro.Autotune_enable = 1

    
    #time.sleep(10)
    print(" Adaptive tuning" )
    myEuro.Adaptive_tune_enable=1
    myEuro.Adaptive_tune_trigger_level=1


    #time.sleep(3)
    if myEuro.setpoint != targetTemp:
        myEuro.setpoint = targetTemp
    
    mylogger = templogger.logger(myeurotherm=myEuro,sleepTime=0.1, showPower=False)

    #mylogger.callbacks.append(printTemp(myEuro))
    
    plt.show()
    
    # myEuro.Ramp_Rate_Disable = 1
    # time.sleep(0.5)
    # myEuro.setpoint = 20
    # time.sleep(0.5)
    # myEuro.Ramp_Rate_Disable = 0
    # time.sleep(0.5)

    
    print(str(myEuro.pid))

    #myEuro.Adaptive_tune_enable=0
    
    #plt.savefig(time.strftime("%Y-%m-%d_%H-%M-%S_ID24_plot.png"))
    mylogger.saveData()
    
    #mylogger.myEuro.setpoint =20
    
